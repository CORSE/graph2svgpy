# Graph2svgpy

This repo contains python bindings (work in progress) for the rust library [graph2svg](https://gitlab.inria.fr/CORSE/graph2svg).

It depends on the maturin tool from the [pyo3 library](https://github.com/PyO3/pyo3).

# Requirement

## Debian/Ubuntu

```bash
# rustup to install rust tool chain
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# install some system requirement for managing font + the solver
sudo apt install libfontconfig1-dev fonts-jetbrains-mono z3
```

# How to use it ?

## From sources

1. Configure your venv (for example with pyenv):

```bash
pyenv install 3.11
pyenv virtualenv 3.11 venv-graph2svgpy
pyenv local venv-graph2svgpy
```

2. Install the maturin tool : `pip install maturin` (might need to `pip install patchelf`)
3. Build the lib and bindings : `maturin develop`
4. Test the installation process :

```bash
python -c "import graph2svgpy; graph2svgpy.hello_world()"
firefox /tmp/test.svg
```

5. Write your own visualisation (see `examples/` for inspirations).

## Maybe one day From Pypy

**TODO one day publish to Pypy**

# Documentation

Currently, due to pyo3 limitation, python type hints are provided by hand within the `graph2svgpy.pyi` file. So when updating the bindings we also should take care of these annotations. This is the best kind of documentation that we have for now in addition to `examples/`.

Also, the [rustdoc documentation](https://gitlab.inria.fr/CORSE/graph2svg#documentation) of the original library and the rust examples can help.
