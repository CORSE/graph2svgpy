use std::{collections::HashMap, hash::Hash};

pub fn adapt_opt_vec<T, TPY: Into<T>>(opt: Option<Vec<TPY>>) -> Vec<T> {
    opt.map(adapt_vec).unwrap_or_default()
}

pub fn adapt_vec<T, TPY: Into<T>>(vec: Vec<TPY>) -> Vec<T> {
    vec.into_iter().map(|val| val.into()).collect()
}

pub fn adapt_map_values<T, TPY: Into<T>, K: Hash + Eq>(map: HashMap<K, TPY>) -> HashMap<K, T> {
    map.into_iter().map(|(k, val)| (k, val.into())).collect()
}

pub fn adapt_opt_map_values<T, TPY: Into<T>, K: Hash + Eq>(
    opt_map: Option<HashMap<K, TPY>>,
) -> HashMap<K, T> {
    opt_map.map(adapt_map_values).unwrap_or_default()
}

pub fn adapt_vec_vec<T, TPY: Into<T>>(vec: Vec<Vec<TPY>>) -> Vec<Vec<T>> {
    vec.into_iter().map(adapt_vec).collect()
}
