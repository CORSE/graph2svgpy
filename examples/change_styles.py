from graph2svgpy import (
    color_str,
    create_svg,
    array2d_nest,
    default_text_class,
    new_container,
    shape_rounded_rect,
    text_boxed,
    shape_sharp_rect,
    same_width,
    align_top_to_bottom,
    ColorTheme,
    Styler,
    rectangle_style,
)


def main():
    redcell = "redcell"
    dashed = "dashed_rect"
    id_header, id_array = 0, 1
    id_container = 100
    n_i, n_j = 4, 4
    red_i, red_j = 2, 1

    # create 2D array
    array = []
    id = id_array + 1
    for _ in range(n_i):
        line = []
        for _ in range(n_j):
            cell = text_boxed(id, "lul", shape_sharp_rect())
            line.append(cell)
            id += 1
        array.append(line)

    # styling options to make change color of one array cell
    styler = Styler(
        theme=ColorTheme.CatppuccinMocha,
        styles={
            redcell: rectangle_style(fill=color_str("red")),
            dashed: rectangle_style(stroke_dasharray="3"),
        },
        nodes_shape_text={
            array[red_i][red_j].id(): (redcell, default_text_class()),
            id_container: (dashed, default_text_class()),
        },
    )

    nodes = [
        array2d_nest(id_array, array, margin_x=0, margin_y=0),
        text_boxed(id_header, "Header", shape=shape_sharp_rect()),
    ]
    container = new_container(id_container, nodes, shape=shape_rounded_rect())
    create_svg(
        "/tmp/test.svg",
        [container],
        constraints=[align_top_to_bottom([id_header, id_array])],
        dims_constraints=[same_width(id_header, id_array)],
        local_safety_dists={(id_header, id_array): 0},
        styler=styler,
    )


if __name__ == "__main__":
    main()
