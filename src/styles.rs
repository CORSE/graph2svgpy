use graph2svg::node::NodeID;
use graph2svg::style::{ColorDict, DEFAULT_FONT, DEFAULT_TEXT};
use pyo3::prelude::*;
use std::collections::HashMap;

use crate::utils::{adapt_opt_map_values, adapt_opt_vec};

const NO_COLOR: Color = Color(graph2svg::style::Color::None);
const COLOR_EDGE: Color = Color(graph2svg::style::COLOR_EDGE);
const BACKGROUND: Color = Color(graph2svg::style::BACKGROUND_COLOR);
const COLOR_RECT_INSIDE: Color = Color(graph2svg::style::COLOR_RECT_INSIDE);
const COLOR_RECT_BORDER: Color = Color(graph2svg::style::COLOR_RECT_BORDER);

#[pyclass(unsendable)]
#[derive(Debug, Clone, Default)]
pub struct Styler(graph2svg::style::Styler);

impl Into<graph2svg::style::Styler> for Styler {
    fn into(self) -> graph2svg::style::Styler {
        self.0
    }
}

#[pymethods]
impl Styler {
    /// Create a new style using [Default] values.
    #[new]
    #[pyo3(signature = (theme = ColorTheme::default(), styles=None, markers=None, nodes_shape_text = None, edges_shape_text_marker = None))]
    pub fn new(
        theme: ColorTheme,
        styles: Option<HashMap<String, Style>>,
        markers: Option<Vec<ArrowMarker>>,
        nodes_shape_text: Option<HashMap<NodeID, (String, String)>>,
        edges_shape_text_marker: Option<HashMap<(NodeID, NodeID), [String; 4]>>,
    ) -> Self {
        Styler(
            graph2svg::style::Styler::new_theme(theme.into())
                .with_styles(adapt_opt_map_values(styles))
                .add_markers(adapt_opt_vec(markers))
                .style_nodes(nodes_shape_text.unwrap_or_default())
                .style_edges(edges_shape_text_marker.unwrap_or_default()),
        )
    }
}

#[pyclass(unsendable)]
#[derive(Debug, Clone)]
pub struct Color(graph2svg::style::Color);

#[pyclass(unsendable)]
#[derive(Default, Debug, Clone, Copy)]
pub enum ColorTheme {
    #[default]
    CatppuccinLatte,
    CatppuccinMocha,
}

impl Into<graph2svg::style::ColorTheme> for ColorTheme {
    fn into(self) -> graph2svg::style::ColorTheme {
        type F = graph2svg::style::ColorTheme;
        match self {
            Self::CatppuccinLatte => F::CatppuccinLatte,
            Self::CatppuccinMocha => F::CatppuccinMocha,
        }
    }
}

#[pymethods]
impl ColorTheme {
    pub fn get_colors_dict(&self) -> ColorDict {
        let theme: graph2svg::style::ColorTheme = (*self).into();
        theme.get_colors_dict()
    }
}

#[pyfunction]
pub fn color_rgb(red: u8, green: u8, blue: u8) -> Color {
    Color(graph2svg::style::Color::RGB(red, green, blue))
}

/// Create a color from a String.
/// Color are used internally to work with themes so if the name you put is a known color of the
/// theme you use, the value written in the css style will be the color hex of the theme.
/// If the name is unknown the value will be written directly to css so hex color code is ok.
/// # Examples
/// - `color_string("red")`
/// - `color_string("#ff11ae)`
#[pyfunction]
pub fn color_str(name_or_hex: String) -> Color {
    Color(graph2svg::style::Color::NameString(name_or_hex))
}

#[pyclass(unsendable)]
#[derive(Debug, Clone)]
pub struct ArrowMarker(graph2svg::style::ArrowMarker);

impl Into<graph2svg::style::ArrowMarker> for ArrowMarker {
    fn into(self) -> graph2svg::style::ArrowMarker {
        self.0
    }
}

#[pymethods]
impl ArrowMarker {
    #[new]
    #[pyo3(signature = (id, width=6, height=6, ref_x=5, ref_y=3, fill= COLOR_EDGE, is_rev = false))]
    fn new(
        id: String,
        width: u8,
        height: u8,
        ref_x: u8,
        ref_y: u8,
        fill: Color,
        is_rev: bool,
    ) -> Self {
        Self(graph2svg::style::ArrowMarker {
            id,
            width,
            height,
            ref_x,
            ref_y,
            fill: fill.0,
            is_rev,
        })
    }
}

#[pyclass(unsendable)]
#[derive(Debug, Clone)]
pub struct FillStroke(graph2svg::style::FillStroke);

#[pymethods]
impl FillStroke {
    #[new]
    #[pyo3(signature = (fill = COLOR_RECT_INSIDE, fill_opacity = 1.0, stroke = COLOR_RECT_BORDER, stroke_opacity=1.0, stroke_width = 1, stroke_dasharray=String::default()))]
    pub fn new(
        fill: Color,
        fill_opacity: f32,
        stroke: Color,
        stroke_opacity: f32,
        stroke_width: usize,
        stroke_dasharray: String,
    ) -> Self {
        FillStroke(graph2svg::style::FillStroke {
            fill: fill.0,
            fill_opacity,
            stroke: stroke.0,
            stroke_opacity,
            stroke_width,
            stroke_dasharray,
        })
    }
}

#[pyclass(unsendable)]
#[derive(Debug, Clone, Default)]
pub enum FontWeight {
    #[default]
    Normal,
    Bold,
}

impl Into<graph2svg::style::FontWeight> for FontWeight {
    fn into(self) -> graph2svg::style::FontWeight {
        type F = graph2svg::style::FontWeight;
        match self {
            Self::Bold => F::Bold,
            Self::Normal => F::Normal,
        }
    }
}

#[pyclass(unsendable)]
#[derive(Debug, Clone, Default)]
pub enum FontStyle {
    #[default]
    Normal,
    Italic,
    Oblique,
}

impl Into<graph2svg::style::FontStyle> for FontStyle {
    fn into(self) -> graph2svg::style::FontStyle {
        type F = graph2svg::style::FontStyle;
        match self {
            Self::Normal => F::Normal,
            Self::Italic => F::Italic,
            Self::Oblique => F::Oblique,
        }
    }
}

#[pyclass(unsendable)]
#[derive(Debug, Clone, Default)]
pub enum DominantBaseline {
    Hanging,
    #[default]
    Middle,
    TextBottom,
}

impl Into<graph2svg::style::DominantBaseline> for DominantBaseline {
    fn into(self) -> graph2svg::style::DominantBaseline {
        type F = graph2svg::style::DominantBaseline;
        match self {
            Self::Hanging => F::Hanging,
            Self::Middle => F::Middle,
            Self::TextBottom => F::TextBottom,
        }
    }
}

#[pyclass(unsendable)]
#[derive(Debug, Clone, Default)]
pub enum TextAnchor {
    Start,
    #[default]
    Middle,
    End,
}
impl Into<graph2svg::style::TextAnchor> for TextAnchor {
    fn into(self) -> graph2svg::style::TextAnchor {
        type F = graph2svg::style::TextAnchor;
        match self {
            Self::Start => F::Start,
            Self::Middle => F::Middle,
            Self::End => F::End,
        }
    }
}

#[pyclass(unsendable)]
#[derive(Debug, Clone)]
pub struct TextStyle(graph2svg::style::TextStyle);

impl Into<graph2svg::style::TextStyle> for TextStyle {
    fn into(self) -> graph2svg::style::TextStyle {
        self.0
    }
}

#[pymethods]
impl TextStyle {
    #[new]
    #[pyo3(signature = (font = DEFAULT_FONT.to_owned(), font_size=12, font_weight= FontWeight::default(), font_style= FontStyle::default(), dominant_baseline= DominantBaseline::default(), text_anchor = TextAnchor::default(), colors = FillStroke(graph2svg::style::FillStroke::text_default())))]
    pub fn new(
        font: String,
        font_size: usize,
        font_weight: FontWeight,
        font_style: FontStyle,
        dominant_baseline: DominantBaseline,
        text_anchor: TextAnchor,
        colors: FillStroke,
    ) -> Self {
        Self(graph2svg::style::TextStyle {
            font,
            font_size,
            font_weight: font_weight.into(),
            font_style: font_style.into(),
            dominant_baseline: dominant_baseline.into(),
            text_anchor: text_anchor.into(),
            colors: colors.0,
        })
    }
}

#[pyclass(unsendable)]
#[derive(Debug, Clone)]
pub struct Style(graph2svg::style::Style);

impl Into<graph2svg::style::Style> for Style {
    fn into(self) -> graph2svg::style::Style {
        self.0
    }
}

#[pyfunction]
pub fn wrap_text_style(text_style: TextStyle) -> Style {
    Style(graph2svg::style::Style::Text(text_style.0))
}

#[pyfunction]
pub fn default_text_class() -> String {
    DEFAULT_TEXT.to_owned()
}

#[pyfunction]
#[pyo3(signature = (font = DEFAULT_FONT.to_owned(), font_size=12, font_weight= FontWeight::default(), font_style= FontStyle::default(), dominant_baseline= DominantBaseline::default(), text_anchor = TextAnchor::default(), colors =FillStroke(graph2svg::style::FillStroke::text_default())))]
pub fn text_style(
    font: String,
    font_size: usize,
    font_weight: FontWeight,
    font_style: FontStyle,
    dominant_baseline: DominantBaseline,
    text_anchor: TextAnchor,
    colors: FillStroke,
) -> Style {
    Style(graph2svg::style::Style::Text(graph2svg::style::TextStyle {
        font,
        font_size,
        font_weight: font_weight.into(),
        font_style: font_style.into(),
        dominant_baseline: dominant_baseline.into(),
        text_anchor: text_anchor.into(),
        colors: colors.0,
    }))
}

#[pyfunction]
#[pyo3(signature = (fill = NO_COLOR, fill_opacity = 0.0, stroke = COLOR_RECT_BORDER, stroke_opacity=1.0, stroke_width = 1, stroke_dasharray=String::default()))]
pub fn edge_style(
    fill: Color,
    fill_opacity: f32,
    stroke: Color,
    stroke_opacity: f32,
    stroke_width: usize,
    stroke_dasharray: String,
) -> Style {
    Style(graph2svg::style::Style::Edge(
        graph2svg::style::FillStroke {
            fill: fill.0,
            fill_opacity,
            stroke: stroke.0,
            stroke_opacity,
            stroke_width,
            stroke_dasharray,
        },
    ))
}

#[pyfunction]
#[pyo3(signature = (fill = COLOR_RECT_INSIDE, fill_opacity = 1.0, stroke = COLOR_RECT_BORDER, stroke_opacity=1.0, stroke_width = 1, stroke_dasharray = String::default()))]
pub fn circle_style(
    fill: Color,
    fill_opacity: f32,
    stroke: Color,
    stroke_opacity: f32,
    stroke_width: usize,
    stroke_dasharray: String,
) -> Style {
    Style(graph2svg::style::Style::Circle(
        graph2svg::style::FillStroke {
            fill: fill.0,
            fill_opacity,
            stroke: stroke.0,
            stroke_opacity,
            stroke_width,
            stroke_dasharray,
        },
    ))
}

#[pyfunction]
#[pyo3(signature = (fill = COLOR_RECT_INSIDE, fill_opacity = 1.0, stroke = COLOR_RECT_BORDER, stroke_opacity=1.0, stroke_width = 1, stroke_dasharray = String::default()))]
pub fn rectangle_style(
    fill: Color,
    fill_opacity: f32,
    stroke: Color,
    stroke_opacity: f32,
    stroke_width: usize,
    stroke_dasharray: String,
) -> Style {
    Style(graph2svg::style::Style::Rect(
        graph2svg::style::FillStroke {
            fill: fill.0,
            fill_opacity,
            stroke: stroke.0,
            stroke_opacity,
            stroke_width,
            stroke_dasharray,
        },
    ))
}

#[pyfunction]
#[pyo3(signature = (background = BACKGROUND))]
pub fn svg_style(background: Color) -> Style {
    Style(graph2svg::style::Style::Svg {
        background: background.0,
    })
}
