use graph2svg::node::NodeID;
use pyo3::prelude::*;

#[pyclass(unsendable)]
#[derive(Debug, Clone)]
pub struct Constraint(graph2svg::Constraint);

#[pyclass(unsendable)]
#[derive(Debug, Clone)]
pub struct DimsConstraint(graph2svg::constraints::DimsConstraint);

#[pyfunction]
#[pyo3(signature = (n1, n2))]
pub fn same_width(n1: NodeID, n2: NodeID) -> DimsConstraint {
    DimsConstraint(graph2svg::constraints::DimsConstraint::SameWidth(n1, n2))
}

#[pyfunction]
#[pyo3(signature = (n1, n2))]
pub fn same_height(n1: NodeID, n2: NodeID) -> DimsConstraint {
    DimsConstraint(graph2svg::constraints::DimsConstraint::SameHeight(n1, n2))
}

#[pyfunction]
#[pyo3(signature = (n1, n2))]
pub fn same_dimensions(n1: NodeID, n2: NodeID) -> DimsConstraint {
    DimsConstraint(graph2svg::constraints::DimsConstraint::SameDims(n1, n2))
}

#[pyclass(unsendable)]
#[derive(Debug, Clone, Default)]
pub struct Anchor(graph2svg::constraints::Anchor);

impl Into<graph2svg::constraints::Anchor> for Anchor {
    fn into(self) -> graph2svg::constraints::Anchor {
        self.0
    }
}

impl Into<graph2svg::Constraint> for Constraint {
    fn into(self) -> graph2svg::Constraint {
        self.0
    }
}
impl Into<graph2svg::Constraint> for &Constraint {
    fn into(self) -> graph2svg::Constraint {
        self.0.clone()
    }
}

impl Into<graph2svg::constraints::DimsConstraint> for DimsConstraint {
    fn into(self) -> graph2svg::constraints::DimsConstraint {
        self.0
    }
}

#[pyfunction]
#[pyo3(signature = (nodes, anchor = Anchor::default()))]
pub fn align_h(nodes: Vec<NodeID>, anchor: Anchor) -> Constraint {
    Constraint(graph2svg::Constraint::AlignHorizontal(nodes, anchor.0))
}

#[pyfunction]
#[pyo3(signature = (nodes, anchor = Anchor::default()))]
pub fn align_v(nodes: Vec<NodeID>, anchor: Anchor) -> Constraint {
    Constraint(graph2svg::Constraint::AlignVertical(nodes, anchor.0))
}

#[pyfunction]
#[pyo3(signature = (nodes, anchor = Anchor::default()))]
pub fn align_left_to_right(nodes: Vec<NodeID>, anchor: Anchor) -> Constraint {
    Constraint(graph2svg::Constraint::AlignLeftToRight(nodes, anchor.0))
}

#[pyfunction]
#[pyo3(signature = (nodes, anchor = Anchor::default()))]
pub fn align_top_to_bottom(nodes: Vec<NodeID>, anchor: Anchor) -> Constraint {
    Constraint(graph2svg::Constraint::AlignTopToBottom(nodes, anchor.0))
}

#[pyfunction]
pub fn order_left_to_right(nodes: Vec<NodeID>) -> Constraint {
    Constraint(graph2svg::Constraint::LeftToRight(nodes))
}

#[pyfunction]
pub fn order_top_to_bottom(nodes: Vec<NodeID>) -> Constraint {
    Constraint(graph2svg::Constraint::TopToBottom(nodes))
}

#[pyfunction]
#[pyo3(signature = (nodes, segment, *,anchor = Anchor::default(), anchor_seg = Anchor::default()))]
pub fn align_h_with_v_segment(
    nodes: Vec<NodeID>,
    segment: (NodeID, NodeID),
    anchor: Anchor,
    anchor_seg: Anchor,
) -> Constraint {
    Constraint(graph2svg::Constraint::AlignHorizontalSegment(
        nodes,
        anchor.0,
        segment,
        anchor_seg.0,
    ))
}

#[pyfunction]
#[pyo3(signature = (nodes, segment, *,anchor = Anchor::default(), anchor_seg = Anchor::default()))]
pub fn align_v_with_h_segment(
    nodes: Vec<NodeID>,
    segment: (NodeID, NodeID),
    anchor: Anchor,
    anchor_seg: Anchor,
) -> Constraint {
    Constraint(graph2svg::Constraint::AlignVerticalSegment(
        nodes,
        anchor.0,
        segment,
        anchor_seg.0,
    ))
}
