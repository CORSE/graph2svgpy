use graph2svg::{node::NodeID, DEFAULT_SAFETY_DIST};
use pyo3::prelude::*;

use crate::utils::{adapt_vec, adapt_vec_vec};

#[pyclass(unsendable)]
#[derive(Debug, Clone)]
pub struct Node(graph2svg::Node);

#[pyclass(unsendable)]
#[derive(Debug, Clone)]
pub struct Shape(graph2svg::Shape);

const DEFAULT_RECT_ROUND: Shape = Shape(graph2svg::Shape::Rectangle((5, 5)));
const NO_SHAPE: Shape = Shape(graph2svg::Shape::None);

#[pyfunction]
#[pyo3(signature = (rounded_x = 5, rounded_y=5))]
pub fn shape_rounded_rect(rounded_x: u32, rounded_y: u32) -> Shape {
    Shape(graph2svg::Shape::Rectangle((rounded_x, rounded_y)))
}

#[pyfunction]
pub fn shape_sharp_rect() -> Shape {
    Shape(graph2svg::Shape::Rectangle((0, 0)))
}

#[pyfunction]
#[pyo3(signature = ())]
pub fn shape_circle() -> Shape {
    Shape(graph2svg::Shape::Circle)
}

#[pyfunction]
pub fn shape_none() -> Shape {
    Shape(graph2svg::Shape::None)
}

#[pymethods]
impl Node {
    pub fn id(&self) -> NodeID {
        self.0.id()
    }
}

impl Into<graph2svg::Node> for Node {
    fn into(self) -> graph2svg::Node {
        self.0
    }
}

impl Into<graph2svg::Node> for &Node {
    fn into(self) -> graph2svg::Node {
        self.0.clone()
    }
}

const MARGIN_X: u32 = 10;
const MARGIN_Y: u32 = 10;

/// Create a leaf node with text inside.
#[pyfunction]
#[pyo3(signature = (id, text, shape= DEFAULT_RECT_ROUND, margin_x= MARGIN_X, margin_y = MARGIN_Y, text_vertical = false))]
pub fn text_boxed(
    id: NodeID,
    text: String,
    shape: Shape,
    margin_x: u32,
    margin_y: u32,
    text_vertical: bool,
) -> Node {
    let node = if text_vertical {
        graph2svg::node::vertical_text_boxed(id, text)
    } else {
        graph2svg::node::text_boxed(id, text)
    };
    Node(node.with_shape(shape.0).with_margin(margin_x, margin_y))
}

/// Create a [Node] containing other [Node]s.
#[pyfunction]
#[pyo3(signature = (id, children, safety_dist=DEFAULT_SAFETY_DIST ,shape= NO_SHAPE, margin_x= MARGIN_X, margin_y = MARGIN_Y))]
pub fn new_container(
    id: NodeID,
    children: Vec<Node>,
    safety_dist: usize,
    shape: Shape,
    margin_x: u32,
    margin_y: u32,
) -> Node {
    let content = graph2svg::node::NodeContent::Container(adapt_vec(children), safety_dist);
    Node(graph2svg::Node::new(id, content, shape.0).with_margin(margin_x, margin_y))
}

/// Helper to create a Node with array2d as content, the content of the array2d should be strings.
#[pyfunction]
#[pyo3(signature = (base_id, cells, cell_margin_x = 0, cell_margin_y = 0, shape= NO_SHAPE, margin_x= MARGIN_X, margin_y = MARGIN_Y))]
pub fn array2d_leafs(
    base_id: NodeID,
    cells: Vec<Vec<String>>,
    cell_margin_x: u32,
    cell_margin_y: u32,
    shape: Shape,
    margin_x: u32,
    margin_y: u32,
) -> Node {
    Node(
        graph2svg::node::array2d_cell_margin(base_id, cells, cell_margin_x, cell_margin_y)
            .with_shape(shape.0)
            .with_margin(margin_x, margin_y),
    )
}

/// Helper to create a Node with array2d as content, the content of the array2d can be any other
/// [Node].
#[pyfunction]
#[pyo3(signature = (base_id, cells, cell_margin_x = 0, cell_margin_y = 0, shape= NO_SHAPE, margin_x= MARGIN_X, margin_y = MARGIN_Y))]
pub fn array2d_nest(
    base_id: NodeID,
    cells: Vec<Vec<Node>>,
    cell_margin_x: u32,
    cell_margin_y: u32,
    shape: Shape,
    margin_x: u32,
    margin_y: u32,
) -> Node {
    let content =
        graph2svg::node::NodeContent::Array2D(adapt_vec_vec(cells), (cell_margin_x, cell_margin_y));
    Node(graph2svg::Node::new(base_id, content, shape.0).with_margin(margin_x, margin_y))
}
