from graph2svgpy import (
    text_boxed,
    create_svg,
    new_container,
    shape_circle,
    shape_sharp_rect,
    arrow,
    align_left_to_right,
)


def main():
    id_nest1, id_nest2 = 10, 20
    id_coucou, id_bonsoir = 11, 12
    id_bye, id_long = 21, 22
    nest1 = [text_boxed(id_coucou, "Coucou"), text_boxed(id_bonsoir, "Bonsoir")]
    nest2 = [text_boxed(id_bye, "Bye"), text_boxed(id_long, "So Long")]
    nodes = [
        new_container(id_nest1, nest1, shape=shape_sharp_rect()),
        new_container(id_nest2, nest2, shape=shape_circle()),
    ]
    edges = [
        arrow(id_coucou, id_bonsoir),
        arrow(id_bonsoir, id_bye),
        arrow(id_bye, id_long),
    ]
    constraints = [
        align_left_to_right([id_nest1, id_nest2]),
        align_left_to_right([id_coucou, id_bonsoir, id_bye, id_long]),
    ]
    create_svg("/tmp/test.svg", nodes, edges, constraints)


if __name__ == "__main__":
    main()
