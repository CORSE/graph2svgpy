mod constraints;
mod edges;
mod nodes;
mod styles;
mod utils;

use std::collections::HashMap;

use constraints::*;
use edges::*;
use graph2svg::{node::NodeID, Graph, DEFAULT_SAFETY_DIST};
use nodes::*;
use pyo3::prelude::*;
use styles::*;
use utils::*;

/// Main entry point to create a graph using the python API. It needs list of [Node], and
/// optionnaly a list of [Edge] to connect the nodes, and optionnaly a list of [Constraint] to
/// force some positionning of nodes.
#[pyfunction]
#[pyo3(signature = (path, nodes, edges=None, constraints=None, dims_constraints=None ,edge_policy=EdgeLinkPolicy::default(), safety_dist=DEFAULT_SAFETY_DIST,local_safety_dists=None, margin_x=10, margin_y=10, styler=None))]
fn create_svg(
    path: &str,
    nodes: Vec<Node>,
    edges: Option<Vec<Edge>>,
    constraints: Option<Vec<Constraint>>,
    dims_constraints: Option<Vec<DimsConstraint>>,
    edge_policy: EdgeLinkPolicy,
    safety_dist: usize,
    local_safety_dists: Option<HashMap<(NodeID, NodeID), usize>>,
    margin_x: u32,
    margin_y: u32,
    styler: Option<Styler>,
) {
    Graph::new(adapt_vec(nodes))
        .add_edges(adapt_opt_vec(edges))
        .add_constraints(adapt_opt_vec(constraints))
        .add_dims_constraints(adapt_opt_vec(dims_constraints))
        .with_safety_dist(safety_dist)
        .with_locals_safety_dist(local_safety_dists.unwrap_or_default())
        .with_edge_policy(edge_policy.into())
        .with_root_margin(margin_x, margin_y)
        .with_styler(styler.unwrap_or_default().into())
        .to_svg(path)
}

/// Create a control flow graph from a custom format using Basic Blocks as list of string
/// representing the instructions inside, control flow describded by a list of pair of ids, and
/// data dependancy as a list of pair of pair of basic block id and instruction line number.
#[pyfunction]
#[pyo3(signature = (path, basic_blocks, meta_edges, intra_edges, constraints=None, dims_constraints=None ,edge_policy=EdgeLinkPolicy::default(), safety_dist=DEFAULT_SAFETY_DIST,local_safety_dists=None, margin_x=10, margin_y=10, styler=None))]
fn create_cfg(
    path: &str,
    basic_blocks: HashMap<NodeID, Vec<&str>>,
    meta_edges: Vec<(NodeID, NodeID)>,
    intra_edges: Vec<((NodeID, usize), (NodeID, usize))>,
    constraints: Option<Vec<Constraint>>,
    dims_constraints: Option<Vec<DimsConstraint>>,
    edge_policy: EdgeLinkPolicy,
    safety_dist: usize,
    local_safety_dists: Option<HashMap<(NodeID, NodeID), usize>>,
    margin_x: u32,
    margin_y: u32,
    styler: Option<Styler>,
) {
    Graph::new_cfg(
        basic_blocks,
        meta_edges,
        intra_edges,
        adapt_opt_vec(constraints),
    )
    .add_dims_constraints(adapt_opt_vec(dims_constraints))
    .with_safety_dist(safety_dist)
    .with_locals_safety_dist(local_safety_dists.unwrap_or_default())
    .with_edge_policy(edge_policy.into())
    .with_root_margin(margin_x, margin_y)
    .with_styler(styler.unwrap_or_default().into())
    .to_svg(path)
}

/// Create a dummy example svg in /tmp/test.svg to test if the bindings works.
#[pyfunction]
fn hello_world() {
    Graph::new(vec![
        graph2svg::text_boxed(0, "Hello"),
        graph2svg::node::text_circled(1, "World!"),
    ])
    .add_constraints(vec![graph2svg::constraints::align_left_to_right(vec![
        0, 1,
    ])])
    .add_edges(vec![graph2svg::arrow(0, 1)])
    .to_svg("/tmp/test.svg")
}

/// A Python module implemented in Rust providings bindings for graph2svg.
#[pymodule]
fn graph2svgpy(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(hello_world, m)?)?;
    m.add_function(wrap_pyfunction!(create_svg, m)?)?;
    m.add_function(wrap_pyfunction!(create_cfg, m)?)?;
    // mod constraints
    m.add_class::<Constraint>()?;
    m.add_class::<DimsConstraint>()?;
    m.add_function(wrap_pyfunction!(same_width, m)?)?;
    m.add_function(wrap_pyfunction!(same_height, m)?)?;
    m.add_function(wrap_pyfunction!(same_dimensions, m)?)?;
    m.add_function(wrap_pyfunction!(align_h, m)?)?;
    m.add_function(wrap_pyfunction!(align_v, m)?)?;
    m.add_function(wrap_pyfunction!(align_left_to_right, m)?)?;
    m.add_function(wrap_pyfunction!(align_top_to_bottom, m)?)?;
    m.add_function(wrap_pyfunction!(align_h_with_v_segment, m)?)?;
    m.add_function(wrap_pyfunction!(align_v_with_h_segment, m)?)?;
    m.add_function(wrap_pyfunction!(order_left_to_right, m)?)?;
    m.add_function(wrap_pyfunction!(order_top_to_bottom, m)?)?;
    // mod edges
    m.add_class::<Edge>()?;
    m.add_class::<DockSelection>()?;
    m.add_class::<Direction>()?;
    m.add_class::<EdgeLinkPolicy>()?;
    m.add_function(wrap_pyfunction!(arrow, m)?)?;
    m.add_function(wrap_pyfunction!(arrow_bi, m)?)?;
    m.add_function(wrap_pyfunction!(edge, m)?)?;
    // mod nodes
    m.add_class::<Node>()?;
    m.add_function(wrap_pyfunction!(text_boxed, m)?)?;
    m.add_function(wrap_pyfunction!(shape_circle, m)?)?;
    m.add_function(wrap_pyfunction!(shape_none, m)?)?;
    m.add_function(wrap_pyfunction!(shape_rounded_rect, m)?)?;
    m.add_function(wrap_pyfunction!(shape_sharp_rect, m)?)?;
    m.add_function(wrap_pyfunction!(new_container, m)?)?;
    m.add_function(wrap_pyfunction!(array2d_nest, m)?)?;
    m.add_function(wrap_pyfunction!(array2d_leafs, m)?)?;
    // mod styles    sm.add_function(wrap_pyfunction!(svg_style, sm)?)?;
    m.add_function(wrap_pyfunction!(rectangle_style, m)?)?;
    m.add_function(wrap_pyfunction!(edge_style, m)?)?;
    m.add_function(wrap_pyfunction!(svg_style, m)?)?;
    m.add_function(wrap_pyfunction!(circle_style, m)?)?;
    m.add_function(wrap_pyfunction!(text_style, m)?)?;
    m.add_function(wrap_pyfunction!(wrap_text_style, m)?)?;
    m.add_function(wrap_pyfunction!(default_text_class, m)?)?;
    m.add_function(wrap_pyfunction!(color_rgb, m)?)?;
    m.add_function(wrap_pyfunction!(color_str, m)?)?;
    m.add_class::<Style>()?;
    m.add_class::<Styler>()?;
    m.add_class::<TextStyle>()?;
    m.add_class::<Color>()?;
    m.add_class::<ArrowMarker>()?;
    m.add_class::<ColorTheme>()?;
    m.add_class::<DominantBaseline>()?;
    m.add_class::<TextAnchor>()?;
    m.add_class::<FontStyle>()?;
    m.add_class::<FontWeight>()?;
    Ok(())
}
