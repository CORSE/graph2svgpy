from graph2svgpy import (
    Styler,
    create_svg,
    array2d_leafs,
    text_boxed,
    shape_sharp_rect,
    same_width,
    align_top_to_bottom,
    ColorTheme,
)


def main():
    id_header = 1
    id_1 = 10
    array1 = [[str(j + i * (i + 1) // 2) for j in range(i + 1)] for i in range(4)]
    id_2 = id_1 + 1 + sum((len(l) for l in array1))
    array2 = [["lul" for _ in range(3)] for _ in range(2)]
    nodes = [
        array2d_leafs(id_1, array1),
        array2d_leafs(id_2, array2, margin_x=0, margin_y=0),
        text_boxed(id_header, "Header", shape=shape_sharp_rect()),
    ]
    create_svg(
        "/tmp/test.svg",
        nodes,
        constraints=[align_top_to_bottom([id_header, id_2])],
        dims_constraints=[same_width(id_header, id_2)],
        local_safety_dists={(id_header, id_2): 0},
        styler=Styler(theme=ColorTheme.CatppuccinMocha),
    )


if __name__ == "__main__":
    main()
