from graph2svgpy import create_cfg


def main():
    basic_blocks = {
        0x1A70: [
            "push rbp",
            "vmovsd xmm3, xmm0, xmm0",
            "mov r8d, 0x34",
            "mov rbp, rsp",
            "and rsp, 0xffffffffffffffe0",
            "mov dword ptr [rsp - 4], edi",
            "mov dword ptr [rsp - 8], esi",
            "mov r11, qword ptr [rbp + 0x18]",
            "mov r10, qword ptr [rbp + 0x20]",
            "mov r9, qword ptr [rbp + 0x10]",
            "nop word ptr cs:[rax + rax]",
        ],
        0x1AA0: [
            "movsxd rdi, dword ptr [rsp - 4]",
            "movsxd rsi, dword ptr [rsp - 8]",
            "imul rax, rdi, 0x2260",
            "imul rdi, rdi, 0x1c20",
            "add rax, r11",
            "vmulsd xmm2, xmm3, qword ptr [rax + rsi*8]",
            "imul rsi, rsi, 0x1c20",
            "lea rdx, [r9 + rdi]",
            "xor eax, eax",
            "lea rcx, [r10 + rsi]",
            "vbroadcastsd ymm1, xmm2",
            "nop word ptr cs:[rax + rax]",
        ],
        0x1AE0: [
            "vmovupd ymm0, ymmword ptr [rcx + rax]",
            "vfmadd213pd ymm0, ymm1, ymmword ptr [rdx + rax]",
            "vmovupd ymmword ptr [rdx + rax], ymm0",
            "add rax, 0x20",
            "cmp rax, 0x5e0",
            "jne 0x1ae0",
        ],
        0x1AFC: [
            "lea rax, [r9 + rdi + 0x5e0]",
            "vmovupd xmm4, xmmword ptr [rax]",
            "vmovddup xmm0, xmm2",
            "vfmadd132pd xmm0, xmm4, xmmword ptr [r10 + rsi + 0x5e0]",
            "vmovupd xmmword ptr [rax], xmm0",
            "dec r8",
            "jne 0x1aa0",
        ],
        0x1B1F: ["vzeroupper ", "leave ", "ret "],
    }
    control_flow = [
        (0x1A70, 0x1AA0),
        (0x1AA0, 0x1AE0),
        (0x1AE0, 0x1AFC),
        (0x1AE0, 0x1AE0),
        (0x1AFC, 0x1B1F),
        (0x1AFC, 0x1AA0),
    ]
    data_dependancies = [((0x1AA0, 1), (0x1AA0, 6)), ((0x1AA0, 2), (0x1AA0, 7))]
    create_cfg("/tmp/test.svg", basic_blocks, control_flow, data_dependancies)


if __name__ == "__main__":
    main()
