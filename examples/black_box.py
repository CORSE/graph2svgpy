from graph2svgpy import (
    text_boxed,
    create_svg,
    arrow,
    align_h,
    align_top_to_bottom,
    align_left_to_right,
    EdgeLinkPolicy,
)


def main():
    nodes = [
        text_boxed(0, "Black Box"),
        text_boxed(1, "Input 1"),
        text_boxed(2, "Input 2"),
        text_boxed(3, "Input 3"),
        text_boxed(11, "Output 1"),
        text_boxed(12, "Output 2"),
        text_boxed(13, "Output 3"),
    ]

    edges = [
        arrow(1, 0),
        arrow(2, 0),
        arrow(3, 0),
        arrow(0, 11),
        arrow(0, 12),
        arrow(0, 13),
    ]

    constraints = [
        align_h([1, 11]),
        align_left_to_right([2, 0, 12]),
        align_h([3, 13]),
        align_top_to_bottom([1, 2, 3]),
        align_top_to_bottom([11, 12, 13]),
    ]
    create_svg(
        "/tmp/test.svg",
        nodes,
        edges,
        constraints,
        edge_policy=EdgeLinkPolicy.FactorizeIO,
    )


if __name__ == "__main__":
    main()
