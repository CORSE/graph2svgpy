from enum import Enum

def default_text_class() -> str:
    """Default css class name for <text>"""

class Styler:
    """Bindings class to graph2svg::style::Styler the main entry point for changing default styles/add new css classes or locally overides some node css classes."""

    def __init__(
        self,
        theme=ColorTheme.CatppuccinLatte,
        styles: dict[str, Style] | None = None,
        markers: list[ArrowMarker] | None = None,
        nodes_shape_text: dict[int, tuple[str, str]] | None = None,
        edges_shape_text_marker: dict[tuple[int, int], tuple[str, str, str, str]]
        | None = None,
    ) -> None:
        """Create new styler with a color theme, some styles classes to add or overides defaults, markers for arrows, and local modifications for nodes and edges.
        # Details
        - nodes_shape_text: values are css class names for the shape and then the text of each node.
        - edges_shape_text_marker: values are css class names for the shape and then the text label and then marker of each edge.
        """

class Color:
    """Bindings class to graph2svg::style::Color that can be created by 2 way: color_str with name or color_rgb with 3 ints."""

def color_str(name_or_hex: str) -> Color:
    """Create a color with name or hex code like #ff11fe"""

def color_rgb(red: int, green: int, blue: int) -> Color:
    """RGB value from 0 to 255."""

class ColorTheme(Enum):
    """Enum bindings to graph2svg::style::ColorTheme. Currently there is only 2 entries CatppuccinLatte (default, light theme), and CatppuccinMocha (dark theme)."""

    CatppuccinLatte = 0
    CatppuccinMocha = 1

class ArrowMarker:
    """Bindings class to graph2svg::style::ArrowMarker."""

    def __init__(
        self, id: str, width: int, height: int, ref_x: int, ref_y: int, fill: Color
    ) -> None:
        """Create a new marker, the probably most important fields are id and fill to change the arrow color."""

class FillStroke:
    """Bindings class to graph2svg::style::FillStroke"""

    def __init__(
        self,
        fill: Color,
        fill_opacity: float,
        stroke: Color,
        stroke_opacity: float,
        stroke_width: int,
        stroke_dasharray: str,
    ) -> None:
        """Create new FillStroke."""

class FontWeight(Enum):
    """Enum Normal (default) or Bold."""

    Normal = 0
    Bold = 1

class FontStyle(Enum):
    """Enum Normal (default) or Italic or Oblique."""

    Normal = 0
    Italic = 1
    Oblique = 2

class DominantBaseline(Enum):
    """Enum Hanging, Middle (default) or TextBottom."""

    Hanging = 0
    Middle = 1
    TextBottom = 2

class TextAnchor(Enum):
    """Enum Start, Middle (default) or End."""

    Start = 0
    Middle = 1
    End = 2

class TextStyle:
    """Gather all style informations about text to generate a css class."""

    def __init__(
        self,
        font: str = "JetBrainsMono NF",
        font_size: int = 12,
        font_weight: FontWeight = FontWeight.Normal,
        font_style: FontStyle = FontStyle.Normal,
        dominant_baseline: DominantBaseline = DominantBaseline.Middle,
        text_anchor: TextAnchor = TextAnchor.Middle,
        colors: FillStroke = FillStroke(
            color_str("text"), 1.0, color_str("none"), 0.0, 0, ""
        ),
    ) -> None:
        """Create new TextStyle"""

class Style:
    """Generic Style bindings to graph2svg::style::Style. Can be created with text_style, edge_style, wrap_text_style, circle_style, rectangle_style, or svg_style"""

def svg_style(background: Color = color_str("base")) -> Style:
    """Create a svg style."""

def rectangle_style(
    fill: Color = color_str("surface0"),
    fill_opacity: float = 1.0,
    stroke: Color = color_str("overlay0"),
    stroke_opacity: float = 1.0,
    stroke_width: int = 1,
    stroke_dasharray: str = "",
) -> Style:
    """Create a default rectangle colors style.
    # Arguments
        - stroke_dasharray: str is a list of int separated by whitespace (usually just one). See https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/stroke-dasharray for more details.
    """

def circle_style(
    fill: Color = color_str("surface0"),
    fill_opacity: float = 1.0,
    stroke: Color = color_str("overlay0"),
    stroke_opacity: float = 1.0,
    stroke_width: int = 1,
    stroke_dasharray: str = "",
) -> Style:
    """Create a default circle colors style.
    # Arguments
        - stroke_dasharray: str is a list of int separated by whitespace (usually just one). See https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/stroke-dasharray for more details.
    """

def edge_style(
    fill: Color = color_str("none"),
    fill_opacity: float = 0.0,
    stroke: Color = color_str("overlay0"),
    stroke_opacity: float = 1.0,
    stroke_width: int = 1,
    stroke_dasharray: str = "",
) -> Style:
    """Create a default edge colors style.
    # Arguments
    - stroke_dasharray: str is a list of int separated by whitespace (usually just one). See https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/stroke-dasharray for more details.
    """

def text_style(
    font: str = "JetBrainsMono NF",
    font_size: int = 12,
    font_weight: FontWeight = FontWeight.Normal,
    font_style: FontStyle = FontStyle.Normal,
    dominant_baseline: DominantBaseline = DominantBaseline.Middle,
    text_anchor: TextAnchor = TextAnchor.Middle,
    colors: FillStroke = FillStroke(
        color_str("text"), 1.0, color_str("none"), 0.0, 0, ""
    ),
) -> Style:
    """Create a wrapped TextStyle into Style."""

class Node:
    """Bindings class to graph2svg::Node"""

    def id(self) -> int:
        """Get the id of the current Node, that has been created from another way."""

class Shape:
    """Bindings class to graph2svg::Shape"""

def shape_rounded_rect(rounded_x: int = 5, rounded_y: int = 5) -> Shape:
    """Create a rectangle shape with rounded corners."""

def shape_sharp_rect() -> Shape:
    """Create a rectangle shape."""

def shape_circle() -> Shape:
    """Create a circle shape."""

def shape_none() -> Shape:
    """Create an empty shape to avoid boxing text for example."""

def text_boxed(
    id: int,
    text: str,
    shape: Shape = shape_rounded_rect(),
    margin_x: int = 10,
    margin_y: int = 10,
    text_vertical: bool = False,
) -> Node:
    """Create a leaf Node containing text boxed in a given Shape with some margin (box to text)."""

def new_container(
    id: int,
    children: list[Node],
    safety_dist: int = 40,
    shape: Shape = shape_none(),
    margin_x: int = 10,
    margin_y: int = 10,
) -> Node:
    """Create a nested graph containing some children nodes, with a distance (manhattan) minimum between each pairs of them. The nesting will be contained in a box, that can be showed in a Shape (default is no shape)."""

def array2d_leafs(
    base_id: int,
    cells: list[list[str]],
    cell_margin_x: int = 0,
    cell_margin_y: int = 0,
    shape: Shape = shape_none(),
    margin_x: int = 10,
    margin_y: int = 10,
) -> Node:
    """Create a array2d Node with leafs nodes (that will be created) containing some texts. You can change the spacing between each cell with cell_margin_x and y."""

def array2d_nest(
    base_id: int,
    cells: list[list[Node]],
    cell_margin_x: int = 0,
    cell_margin_y: int = 0,
    shape: Shape = shape_none(),
    margin_x: int = 10,
    margin_y: int = 10,
) -> Node:
    """Create a array2d Node with arbitrary nodes (already created) so it can nest. You can change the spacing between each cell with cell_margin_x and y."""

class Constraint:
    """Class bindings to graph2svg::Constraint."""

class DimsConstraint:
    """Class bindings to graph2svg::constraints::DimsConstraint."""

class Anchor(Enum):
    """Python Enum bindings to graph2svg::constraints::Anchor."""

    Start = 0
    Center = 1
    End = 2

def same_width(n1: int, n2: int) -> DimsConstraint:
    """Create DimsConstraint that enforce 2 nodes to got width1 == width2."""

def same_height(n1: int, n2: int) -> DimsConstraint:
    """Create DimsConstraint that enforce 2 nodes to got height1 == height2."""

def same_dims(n1: int, n2: int) -> DimsConstraint:
    """Create DimsConstraint that enforce 2 nodes to got width1 == width2 and height1 == height2."""

def align_h(
    nodes: list[int],
    anchor: Anchor = Anchor.Center,  # pyright:ignore
) -> Constraint:
    """Create Constraint for aligning n > 1 nodes horizontally with a given anchor (default is center)."""

def align_v(
    nodes: list[int],
    anchor: Anchor = Anchor.Center,  # pyright:ignore
) -> Constraint:
    """Create Constraint for aligning n > 1 nodes vertically with a given anchor (default is center)."""

def align_h_with_v_segment(
    nodes: list[int],
    segment: tuple[int, int],
    anchor: Anchor = Anchor.Center,  # pyright:ignore
    anchor_seg: Anchor = Anchor.Center,  # pyright:ignore
) -> Constraint:
    """Create Constraint for aligning n > 1 nodes horizontally with a given anchor (default is center) on a particular point (default is center) of a vertical segment between 2 nodes already aligned this way."""

def align_v_with_h_segment(
    nodes: list[int],
    segment: tuple[int, int],
    anchor: Anchor = Anchor.Center,  # pyright:ignore
    anchor_seg: Anchor = Anchor.Center,  # pyright:ignore
) -> Constraint:
    """Create Constraint for aligning n > 1 nodes vertically with a given anchor (default is center) on a particular point (default is center) of a horizontal segment between 2 nodes already aligned this way."""

def align_left_to_right(
    nodes: list[int], anchor: Anchor = Anchor.Center  # pyright:ignore
) -> Constraint:
    """Create Constraint for aligning n > 1 nodes horizontally with a given anchor (default is center) and enforce x1 < x2 < .. < xn (with respect to widths)."""

def align_top_to_bottom(
    nodes: list[int], anchor: Anchor = Anchor.Center  # pyright:ignore
) -> Constraint:
    """Create Constraint for aligning n > 1 nodes vertically with a given anchor (default is center) and enforce y1 < y2 < .. < yn (with respect to heights)."""

def order_left_to_right(nodes: list[Node]) -> Constraint:
    """Create Constraint enforcing x1 < x2 < .. < xn (with respect to widths)."""

def order_top_to_bottom(nodes: list[Node]) -> Constraint:
    """Create Constraint enforcing y1 < y2 < .. < yn (with respect to heights)."""

class Edge:
    """Class bindings for graph2svg::Edge."""

class EdgeLinkPolicy(Enum):
    """Python Enum bindings for graph2svg::edge::EdgeLinkPolicy. Useful for changing the global policy on how to connect nodes."""

    FactorizeIO = 0
    MaximizeDockPoints = 1

class Direction(Enum):
    """Python Enum bindings for graph2svg::edge::Direction. Useful for creating DockSelection to change locally the points where Edge should start and end."""

    Left = 0
    Right = 1
    Top = 2
    Bottom = 3

class DockSelection:
    """Class bindings to graph2svg::edge::DockSelection. Useful for changing where an Edge should start or end (select a face, select a ratio to divide the face, select the points to keep only some part of the face)."""

    def __init__(
        self,
        diviser: int = 2,
        faces: list[Direction] | None = None,
        selected: list[int] | None = None,
    ):
        """Create a new DockSelection.
        # Args
        - diviser: int, should be at least 2, default 2, number by which you divide the faces length.
        - directions: list[Direction], shouldn't be empty, default is all faces (Left, Right, Top, Bottom).
        - selected: list[int], shouldn't be empty, len less than diviser, default is all points i.e [i for i in range(1,diviser)]
        """

def arrow_bi(
    from_: int,
    to: int,
    from_dock: DockSelection | None = None,
    to_dock: DockSelection | None = None,
    label: str | None = None,
    label_anchor: Anchor = Anchor.Center,
    label_above: bool = False,
    label_vertical: bool = False,
) -> Edge:
    """Create an Edge with 2 default markers at the start and end to be a bidirectionnal arrow."""

def arrow(
    from_: int,
    to: int,
    from_dock: DockSelection | None = None,
    to_dock: DockSelection | None = None,
    label: str | None = None,
    label_anchor: Anchor = Anchor.Center,
    label_above: bool = False,
    label_vertical: bool = False,
) -> Edge:
    """Create an Edge with a default marker at the end to be an arrow."""

def edge(
    from_: int,
    to: int,
    from_dock: DockSelection | None = None,
    to_dock: DockSelection | None = None,
    label: str | None = None,
    label_anchor: Anchor = Anchor.Center,
    label_above: bool = False,
    label_vertical: bool = False,
) -> Edge:
    """Create an Edge without marker at the end."""

def create_svg(
    path: str,
    nodes: list[Node],
    edges: list[Edge] | None = None,
    constraints: list[Constraint] | None = None,
    dims_constraints: list[DimsConstraint] | None = None,
    edge_policy: EdgeLinkPolicy = EdgeLinkPolicy.MaximizeDockPoints,  # pyright:ignore
    safety_dist: int = 40,
    local_safety_dists: dict[tuple[int, int], int] | None = None,
    margin_x: int = 10,
    margin_y: int = 10,
    styler: Styler | None = None,
) -> None:
    """Main method to create svg."""

def create_cfg(
    path: str,
    basic_blocks: dict[int, list[str]],
    meta_edges: list[tuple[int, int]],
    intra_edges: list[tuple[tuple[int, int], tuple[int, int]]],
    constraints: list[Constraint] | None = None,
    dims_constraints: list[DimsConstraint] | None = None,
    edge_policy: EdgeLinkPolicy = EdgeLinkPolicy.MaximizeDockPoints,  # pyright:ignore
    safety_dist: int = 40,
    local_safety_dists: dict[tuple[int, int], int] | None = None,
    margin_x: int = 10,
    margin_y: int = 10,
    styler: Styler | None = None,
) -> None:
    """Create a control flow graph svg."""
