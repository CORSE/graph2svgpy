from graph2svgpy import create_svg, DockSelection, Direction, arrow, array2d_leafs


def main():
    id_base = 0
    n = 6
    content = [[str(i) for i in range(n)]]
    up_right = DockSelection(3, [Direction.Right], [1])
    up_left = DockSelection(3, [Direction.Left], [1])
    down_right = DockSelection(3, [Direction.Right], [2])
    down_left = DockSelection(3, [Direction.Left], [2])

    edges_forwards = [
        arrow(id, id + 1, up_right, up_left) for id in range(id_base + 1, id_base + n)
    ]
    edges_backwards = [
        arrow(id + 1, id, down_left, down_right)
        for id in range(id_base + 1, id_base + n)
    ]
    create_svg(
        "/tmp/test.svg",
        [array2d_leafs(id_base, content, 30)],
        edges_forwards + edges_backwards,
    )


if __name__ == "__main__":
    main()
