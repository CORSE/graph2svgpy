use crate::constraints::Anchor;
use crate::utils::adapt_vec;
use graph2svg::node::NodeID;
use pyo3::prelude::*;

#[pyclass(unsendable)]
#[derive(Debug, Clone, Copy, Default)]
pub enum EdgeLinkPolicy {
    FactorizeIO,
    #[default]
    MaximizeDockPoints,
}

#[pyclass(unsendable)]
#[derive(Debug, Clone, Copy)]
pub enum Direction {
    Left = 0,
    Top = 1,
    Right = 2,
    Bottom = 3,
}

const ALL_DIRECTIONS: [Direction; 4] = [
    Direction::Left,
    Direction::Top,
    Direction::Right,
    Direction::Bottom,
];

impl Into<graph2svg::edge::Direction> for Direction {
    fn into(self) -> graph2svg::edge::Direction {
        type F = graph2svg::edge::Direction;
        match self {
            Self::Left => F::Left,
            Self::Right => F::Right,
            Self::Bottom => F::Bottom,
            Self::Top => F::Top,
        }
    }
}
impl Into<graph2svg::edge::EdgeLinkPolicy> for EdgeLinkPolicy {
    fn into(self) -> graph2svg::edge::EdgeLinkPolicy {
        type F = graph2svg::edge::EdgeLinkPolicy;
        match self {
            Self::FactorizeIO => F::FactorizeIO,
            Self::MaximizeDockPoints => F::MaximizeDockPoints,
        }
    }
}

#[pyclass(unsendable)]
#[derive(Debug, Clone)]
pub struct DockSelection(graph2svg::edge::DockSelection);

#[pymethods]
impl DockSelection {
    /// # Defaults
    /// faces = [left, right, top, bottom]
    /// diviser = 2
    /// selected = [i for i in range(1,diviser)]
    /// i.e choose the center of the 4 faces
    #[new]
    #[pyo3(signature = (diviser=2,faces=None,  selected=None))]
    pub fn new(diviser: i32, faces: Option<Vec<Direction>>, selected: Option<Vec<i32>>) -> Self {
        let directions = adapt_vec(faces.unwrap_or_else(|| ALL_DIRECTIONS.to_vec()));
        let selected = selected.unwrap_or_else(|| (1..diviser).collect());
        Self(graph2svg::edge::DockSelection::faces_and_segments(
            diviser, directions, selected,
        ))
    }
}

#[pyclass(unsendable)]
#[derive(Debug, Clone)]
pub struct Edge(graph2svg::Edge);

impl Edge {
    fn set_all_args(
        mut self,
        from_dock: Option<DockSelection>,
        to_dock: Option<DockSelection>,
        label: Option<String>,
        label_anchor: Anchor,
        label_above: bool,
        label_vertical: bool,
    ) -> Self {
        self.0 = self.0.with_label_anchor(label_anchor.into(), label_above);
        if let Some(from_dock) = from_dock {
            self.0 = self.0.with_from_dock(from_dock.0);
        }
        if let Some(to_dock) = to_dock {
            self.0 = self.0.with_to_dock(to_dock.0);
        }
        if let Some(label) = label {
            self.0 = if label_vertical {
                self.0.with_vertical_label(label)
            } else {
                self.0.with_label(label)
            };
        }
        self
    }
}

impl Into<graph2svg::Edge> for Edge {
    fn into(self) -> graph2svg::Edge {
        self.0
    }
}

impl Into<graph2svg::Edge> for &Edge {
    fn into(self) -> graph2svg::Edge {
        self.0.clone()
    }
}

#[pyfunction]
#[pyo3(signature = (from, to, from_dock=None, to_dock=None, label=None, label_anchor=Anchor::default(), label_above = false, label_vertical = false))]
pub fn arrow(
    from: NodeID,
    to: NodeID,
    from_dock: Option<DockSelection>,
    to_dock: Option<DockSelection>,
    label: Option<String>,
    label_anchor: Anchor,
    label_above: bool,
    label_vertical: bool,
) -> Edge {
    Edge(graph2svg::arrow(from, to)).set_all_args(
        from_dock,
        to_dock,
        label,
        label_anchor,
        label_above,
        label_vertical,
    )
}

#[pyfunction]
#[pyo3(signature = (from, to, from_dock=None, to_dock=None, label=None, label_anchor=Anchor::default(), label_above = false, label_vertical = false))]
pub fn arrow_bi(
    from: NodeID,
    to: NodeID,
    from_dock: Option<DockSelection>,
    to_dock: Option<DockSelection>,
    label: Option<String>,
    label_anchor: Anchor,
    label_above: bool,
    label_vertical: bool,
) -> Edge {
    Edge(graph2svg::edge::arrow_bi(from, to)).set_all_args(
        from_dock,
        to_dock,
        label,
        label_anchor,
        label_above,
        label_vertical,
    )
}

#[pyfunction]
#[pyo3(signature = (from, to, from_dock=None, to_dock=None, label=None, label_anchor=Anchor::default(), label_above = false, label_vertical = false))]
pub fn edge(
    from: NodeID,
    to: NodeID,
    from_dock: Option<DockSelection>,
    to_dock: Option<DockSelection>,
    label: Option<String>,
    label_anchor: Anchor,
    label_above: bool,
    label_vertical: bool,
) -> Edge {
    Edge(graph2svg::edge(from, to)).set_all_args(
        from_dock,
        to_dock,
        label,
        label_anchor,
        label_above,
        label_vertical,
    )
}
